

/******************************************************************
* Copyright 2021, All rights reserved, For internal use only
*
* FILE: HomeOffice.ino
* PROJECT: HomeOfficeAssistant
* MODULE: MAIN-MODULE
*
* Description:
*
* Notes:
*
* Compiler dependencies or special instructions:
*
* REVISION HISTORY
* Date: By: Description:
*
* Version: 1.0.0.1
*
* *****************************************************************/



//weiß 10 
//blau 11
//rot SDA
//braun SCL


////////////////////////////////////////////////////////////////////
//  LIBRARIES                                                     //
////////////////////////////////////////////////////////////////////
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <RTClib.h>           //Used for date and time
#include <SoftwareSerial.h>   //Serial used for CO2 Sensor
#include <MHZ19.h>            //CO2 Sensor
#include <DHT.h>              //Adafruit
#include <Adafruit_NeoPixel.h>  //LED Stripe
#include "pitches.h"


////////////////////////////////////////////////////////////////////
//  PINs                                                          //
////////////////////////////////////////////////////////////////////
#define RX_PIN 10                  // RX & TX used for CO2 sensor communication
#define TX_PIN 11
#define DHT11_PIN 2               //Temp, Hum Sensor
#define BUTTON_MENU 4
#define BUTTON_SOUND 5
#define PIXEL_PIN 6
#define LDR_PIN 0
#define SOUND_PIN 8

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define BAUDRATE 9600

#define DHTTYPE DHT11
#define NUMPIXELS 60
#define PIXELWIDTH 5


//Instances
MHZ19 mhz19b;                                       //CO2 sensor object
SoftwareSerial co2Serial(RX_PIN,TX_PIN);            //Pins used for CO2 sensor
RTC_DS1307 rtc;                                     //RealTimeClock Module
//LiquidCrystal lcd(7, 8, 9, 10, 11, 12);             //LCD DisplayPins
DHT dht(DHT11_PIN, DHTTYPE);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
Adafruit_NeoPixel pixels(NUMPIXELS, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

////////////////////////////////////////////////////////////////////
//  CONSTANT                                                      //
////////////////////////////////////////////////////////////////////



String temp_name = "Raumtemperatur";
String temp_unit = "C";

#define DISPLAY_DELAY  8000
#define WARNING_DELAY  1000
#define DEBOUNCE_DELAY  200
#define SENSOR_DELAY    500
#define SOUND_DELAY    2000
#define LUX_DELAY      2000

// Type in the threshold for each parameter
#define MAX_TEMP   30.00  //°C
#define MIN_TEMP   19.00  //°C
#define MAX_HUM    40.00  // %
#define MIN_HUM    10.00  // %
#define MIN_LUX   -50  //LUX
#define CO2_GOOD  1000.00 //ppm
#define CO2_BAD   2000.00 //ppm


#define DEGREE "\247 C"






////////////////////////////////////////////////////////////////////
//  GLOBAL VARIABLES                                              //
////////////////////////////////////////////////////////////////////
unsigned long warning_millis = 0;
unsigned long display_millis = 0;
unsigned long debounce_millis = 0;
unsigned long humidity_millis = 0;
unsigned long temperature_millis = 0;
unsigned long CO2_millis = 0;
unsigned long sound_millis = 0;
unsigned long lux_millis = 0;
unsigned long deb_millis = 0;
unsigned long button_millis = 0;

float CO2 = 0;
float temperature = 0;
float humidity = 0;
float lux = 0;


int menu_index = 1;
bool warning_flag = false;
bool button_state_sound = false;
bool button_state = false;
bool prev_button_result = false;
int sound_on = 1;
bool play_sound = false;
bool sound_flag = false;
int pixel_index = 0;
int sound_duration = 500;
const uint32_t GREEN = pixels.Color(0, 255, 0);
const uint32_t YELLOW = pixels.Color(255, 120, 0);
const uint32_t RED = pixels.Color(255, 0, 0);


////////////////////////////////////////////////////////////////////
//  CLASSES                                                     //
////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////
//  FUNCTIONS                                                     //
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
//      SETUP                                                     //
////////////////////////////////////////////////////////////////////
void pixel_setup() {
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
  #endif

  pixels.begin();
}


void display_setup() {
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
}


void sensor_setup() {
  dht.begin();
  co2Serial.begin(BAUDRATE);
  mhz19b.begin(co2Serial);
  mhz19b.autoCalibration();
}

void RTC_init() {
  while (!Serial); // for Leonardo/Micro/Zero
   
    //Serial.begin(BAUDRATE);
    if (! rtc.begin()) {
      Serial.println(F("Couldn't find RTC"));
      while (1);
    }
   
    if (! rtc.isrunning()) {
      Serial.println(F("RTC is NOT running!"));
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
      Serial.println(F("RTC adjusted!"));
    }
  }






////////////////////////////////////////////////////////////////////
//      SENSORS                                                   //
////////////////////////////////////////////////////////////////////

bool read_all_sensor() {
  bool sensor_error = false;
  if (read_humidity() == false) sensor_error = true;
  if (read_temperature() == false) sensor_error = true;
  if (read_CO2() == false) sensor_error = true;
  read_lux();
  return sensor_error;
}

void read_lux() {
  if ((millis() - lux_millis) > LUX_DELAY) {
      lux = (2500/(analogRead(LDR_PIN)*0.00488759)-500)/10;
      lux_millis = millis();
  }
}

bool read_humidity() {
  if ((millis() - humidity_millis) > SENSOR_DELAY){
    if (isnan(dht.readHumidity()) == false) {
      humidity = dht.readHumidity();
      humidity_millis = millis();
      return true;
    }
    else {
      print_error(F("DHT-11 Sensor, humidity!"));
      return false;
   }
  }
}

bool read_temperature() {
  if ((millis() - temperature_millis) > SENSOR_DELAY){
   if (isnan(dht.readTemperature()) == false) {
    temperature = dht.readTemperature();
    temperature_millis = millis();
    return true;
   }
   else {
     print_error(F("DHT-11 Sensor, temperature!"));
     return false;
   }
  }
}


bool read_CO2() {
  if ((millis() - CO2_millis) > SENSOR_DELAY) {
    if (mhz19b.errorCode == RESULT_OK) {
      CO2 = mhz19b.getCO2();          //Request CO2 (as ppm)
      CO2_millis = millis();
      return true;
    }
    else {
      print_error(F("MHZ19B Sensor, CO2! "));
      return false;
    }
  }

}




bool warning() {
  bool warning_detected = false;
  if (threshold (humidity, "Luftfeuchtigkeit", MIN_HUM, MAX_HUM, "%") ||
      threshold (temperature, "Raumtemperatur", MIN_TEMP, MAX_TEMP, DEGREE) || 
      threshold (lux, "Lichtstärke", MIN_LUX, 9999999, "lx")) warning_detected = true;
  return warning_detected;
}

bool threshold (float &Parameter, String Parameter_Name, float neg_threshold, float pos_threshold, String unit) {
  if (Parameter > pos_threshold) {
    if ((millis() - warning_millis) > WARNING_DELAY ){
      if (warning_flag) {
        display.clearDisplay();
        display.setCursor(12, 2);
        display.setTextSize(1);
        display.print(Parameter_Name);
        display.setTextSize(2);
        display.setCursor(13,13);
        display.print(F("zu hoch!"));
        display.display();
        
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setCursor(2, 7);
        display.setTextSize(3);
        display.print(Parameter);
        //display.print(unit);
        display.display();
        warning_flag = true;
      }
      warning_millis = millis();
    }
    return true;
  }
    
  
  if (Parameter < neg_threshold) {
    if ((millis() - warning_millis) > WARNING_DELAY ){
      if (warning_flag) {
        display.clearDisplay();
        display.setCursor(12, 2);
        display.setTextSize(1);
        display.print(Parameter_Name);
        display.setTextSize(2);
        display.setCursor(0,13);
        display.print(F("zu niedrig"));
        display.display();
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setCursor(2, 7);
        display.setTextSize(3);
        display.print(Parameter);
        //display.print(unit);
        display.display();
        warning_flag = true;
      }
      warning_millis = millis();
    }
    return true;
  }
  

  else return false;
}


uint32_t CO2_trafficLight() {
  if (CO2 <  CO2_GOOD) return GREEN;
  if ((CO2 <= CO2_BAD) && (CO2 >= CO2_GOOD)) return YELLOW;
  if (CO2 > CO2_BAD) return RED;
}





////////////////////////////////////////////////////////////////////
//      LED                                                       //
////////////////////////////////////////////////////////////////////
void fadeToBlack(int ledNo, byte fadeValue) {
#ifdef ADAFRUIT_NEOPIXEL_H
  // NeoPixel
  uint32_t oldColor;
  uint8_t r, g, b;
  int value;

  oldColor = pixels.getPixelColor(ledNo);
  r = (oldColor & 0x00ff0000UL) >> 16;
  g = (oldColor & 0x0000ff00UL) >> 8;
  b = (oldColor & 0x000000ffUL);

  r = (r <= 10) ? 0 : (int) r - (r * fadeValue / 256);
  g = (g <= 10) ? 0 : (int) g - (g * fadeValue / 256);
  b = (b <= 10) ? 0 : (int) b - (b * fadeValue / 256);

  pixels.setPixelColor(ledNo, r, g, b);
#endif
#ifndef ADAFRUIT_NEOPIXEL_H
  // FastLED
  leds[ledNo].fadeToBlackBy( fadeValue );
#endif
}


void pixel_fading(byte meteorSize, byte meteorTrailDecay, boolean meteorRandomDecay, int SpeedDelay, int Speed) {
  unsigned long currentMillisMeteor = millis();
  static unsigned long previousMillisMeteor = 0;
  static byte j = 0;  //<-- warum static? j wird überall mit 0 initialisiert
  if ((currentMillisMeteor - previousMillisMeteor) >= Speed) {
    previousMillisMeteor = currentMillisMeteor;

    //for (int i = 0; i < Anzahl_Pixel + Anzahl_Pixel; i++) {
    static uint16_t i=0;
    i++;
      // fade brightness all LEDs one step
      for (int j = 0; j < NUMPIXELS; j++) {
        if ( (!meteorRandomDecay) || (random(10) > 5) ) {
          fadeToBlack(j, meteorTrailDecay );
        }
      }
      // draw meteor
      for (int j = 0; j < meteorSize; j++) {
        if ( ( i - j < NUMPIXELS) && (i - j >= 0) ) {
          pixels.setPixelColor(i - j, 255, 0, 0);
        }
      }
      pixels.show();
      j++;  //<-- was macht das j++ hier? j wird überall mit 0 neu initialisiert
      //delay(SpeedDelay);
      if (i>= NUMPIXELS)i=0;
    //} //Ende FOR i
  }
}


void all_LED (uint32_t color) {
    
    pixels.clear();
    pixels.fill(color, 0, NUMPIXELS-1);
    pixels.setBrightness(255);
    pixels.show();
}







////////////////////////////////////////////////////////////////////
//      DISPLAY                                                   //
////////////////////////////////////////////////////////////////////
void print_error (String error_message) {
  display.clearDisplay();
  display.print ("Error:" + error_message);
}

/*
bool button_press_menu() {
    bool button_signal = digitalRead(BUTTON_MENU);
    bool button_result = false;
    if (button_signal =! button_state) debounce_millis = millis();
    if ((millis() - button_millis) > DEBOUNCE_DELAY) {
        if (button_signal != button_state) {
            button_state = button_signal;
        }
        if ((button_signal == true) && (prev_button_result == false)) {
            button_result = true;
        }
        else button_result = false;
        prev_button_result = button_result;
        return button_result;
    }
}

*/

void print_information () {

  if (millis() >= display_millis + DISPLAY_DELAY) {
    menu_index++;
    display_millis = millis();
  }
  if((millis() - button_millis) > DEBOUNCE_DELAY) {
    bool button_signal = digitalRead(BUTTON_MENU);
    debounce_millis = millis();
    if ((button_signal == true) && (button_state==false)) {
      menu_index++;
    }
    button_state = button_signal;
  }
  if (menu_index > 5) menu_index = 1;

  
  switch (menu_index) {
     case 1: print_temperature();
     break;
     case 2: print_humidity();
     break;
     case 3: print_date_time();
     break;
     case 4: print_CO2();
     break;
     case 5: print_lux();
     break;
  }
    

  
}


void print_temperature() {
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(15,1);
    display.print(F("Raumtemperatur:"));
    display.setTextSize(3);
    display.setCursor(7, 12);
    display.print(temperature);
    display.print((char)247); //° (Degree)
    display.print(F("C"));
    display.display();
    display.clearDisplay();
}
void print_humidity() {
    display.clearDisplay();
    display.setCursor(15,1);
    display.setTextSize(1);
    display.print (F("Luftfeuchtigkeit:"));
    display.setTextSize(3);
    display.setCursor(7, 12);
    display.print(humidity);
    display.print(F("%"));
    display.display();
    display.clearDisplay();
}
void print_date_time() {

    DateTime now = rtc.now();
    display.clearDisplay();
 //Date
    display.setTextSize(2);
    display.setCursor(5, 0);
    if (now.day() < 10) display.print(0);
    display.print(now.day(), DEC);
    display.print(".");
    if (now.month() < 10) display.print(0);
    display.print(now.month(), DEC);
    display.print(".");
    display.print (now.year(),DEC);
//Time
    display.setCursor(15, 18);
    if (now.hour() < 10) display.print(0);
    display.print(now.hour(), DEC);
    display.print(":");
    if (now.minute() < 10) display.print(0);
    display.print(now.minute(), DEC);
    display.print(":");
    if (now.second() < 10) display.print(0);
    display.print(now.second(), DEC);
    
    display.display();
}


void print_CO2() {
  int co2 = CO2;
  display.clearDisplay();
  display.setCursor(25,0);
  display.setTextSize(1);
  display.print(F("CO2-Gehalt: "));
  display.setTextSize(3);
  display.setCursor(2, 12);
  display.print(co2);
  display.print(F("ppm"));
  display.display();
  display.clearDisplay();
}


void print_lux() {
  int LUX = lux;
  display.clearDisplay();
  display.setCursor(25,0);
  display.setTextSize(1);
  display.print(F("Lichtstärke"));
  display.setTextSize(3);
  display.setCursor(2, 12);
  display.print(LUX);
  display.print(F(" lx"));
  display.display();
  display.clearDisplay();
}


void warning_sound() {
    if (sound_on > 0) {                                         //If sound is switched on && warning=true
        if (play_sound == true) {                               //Sound ist an
            if ((millis() - sound_millis) > SOUND_DELAY ) {     // Timer ist abgelaufen
                play_sound = false;
            }
            else{                                               //Timer nicht abgelaufen
                //play sound
            }
        }
        else if(play_sound == false) {                          // Sound ist aus
            if (sound_flag == false) {                          // schon gespielt?
                sound_millis = millis();
                sound_flag = true;
                play_sound == true;
                tone(SOUND_PIN, NOTE_C6, sound_duration);
            }
        }
    }
    
}



////////////////////////////////////////////////////////////////////
//  MAIN CODE                                                     //
////////////////////////////////////////////////////////////////////







void setup( )
{
  
  Serial.begin( 9600);
  //freeMemory();
  sensor_setup();
  Serial.println(F("solved sensor_setup"));
  display_setup();
  Serial.println(F("solved display_setup"));                             //Setup all Sensors
  //lcd.begin(16, 2);           // set up the LCD's number of columns and rows:
  RTC_init();
  pixel_setup();
  pinMode(BUTTON_MENU, INPUT);
  pinMode(BUTTON_SOUND, INPUT);
  pinMode(LDR_PIN, INPUT);
  display.clearDisplay();
  display.setCursor(1, 1);
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.print(F("Booting..."));
  display.display();

    
  delay(1000);               // to prevent that everything has enough time to boot up correctly
}


void loop() { button_state_sound = digitalRead(BUTTON_SOUND);
      if (button_state_sound == HIGH) {
            if ((millis - deb_millis) > DEBOUNCE_DELAY) {
              sound_on = -sound_on;                                     //Toggle
              display.clearDisplay();
              display.setCursor(1, 1);
              display.setTextSize(2);
              display.setTextColor(SSD1306_WHITE);
              if (sound_on > 0) display.print(F("Sound ON"));
              else display.print(F("Sound OFF"));
              display.display();
              deb_millis = millis();
                                                             // prevent that "Sound ON/OFF" is shown
          }
          delay(1000);
      }
    
    
    if (read_all_sensor() == false) {                           //If all sensor-values are valid
        if (warning() == false) {
            print_information();            //Jump to normal_mode if no warning is detected
            sound_flag = false;
            all_LED(CO2_trafficLight());
        }
            
        else {
            warning_sound();
            pixel_fading(1,10, 0, 0.01, 20);
        }
    }
 
  
}

// change