

/******************************************************************
* Copyright 2021, All rights reserved, For internal use only
*
* FILE: HomeOffice.ino
* PROJECT: HomeOfficeAssistant
* MODULE: MAIN-MODULE
*
* Description:
*
* Notes:
*
* Compiler dependencies or special instructions:
*
* REVISION HISTORY
* Date: By: Description:
*
* Version: 1.0.0.1
*
* *****************************************************************/



//weiß 10 
//blau 11
//rot SDA
//braun SCL


////////////////////////////////////////////////////////////////////
//  LIBRARIES                                                     //
////////////////////////////////////////////////////////////////////
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <RTClib.h>           //Used for date and time
//#include <LiquidCrystal.h>    //LCD Display
#include <SoftwareSerial.h>   //Serial used for CO2 Sensor
#include <MHZ19.h>            //CO2 Sensor
#include <DHT.h>              //Adafruit

#include "Sensor.h"

////////////////////////////////////////////////////////////////////
//  PINs                                                          //
////////////////////////////////////////////////////////////////////
#define RX_PIN 10                  // RX & TX used for CO2 sensor communication
#define TX_PIN 11
#define DHT11_PIN 2               //Temp, Hum Sensor
#define BUTTON_MENU 4
//#define BUTTON_SOUND 5
#define BLUE 3
#define GREEN 5
#define RED 6

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define BAUDRATE 9600

#define DHTTYPE DHT11

//Instances
MHZ19 mhz19b;                                       //CO2 sensor object
SoftwareSerial co2Serial(RX_PIN,TX_PIN);            //Pins used for CO2 sensor
RTC_DS1307 rtc;                                     //RealTimeClock Module
//LiquidCrystal lcd(7, 8, 9, 10, 11, 12);             //LCD DisplayPins
DHT dht(DHT11_PIN, DHTTYPE);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

////////////////////////////////////////////////////////////////////
//  CONSTANT                                                      //
////////////////////////////////////////////////////////////////////



String temp_name = "Raumtemperatur";
String temp_unit = "C";

/*
const unsigned long display_time = 5000;
const unsigned long warning_time = 1000;
const unsigned long serial_time = 20000;
const unsigned long leddim_time = 20;
const unsigned long debounce_time = 50;
const unsigned long dht11_time = 500;
const unsigned long CO2_time = 2000;

const unsigned long Test_time = 3000;
*/
// Type in the threshold for each parameter
 float max_temp =  30.00; //°C
 float min_temp =  13.00; //°C
//const float max_hum  =  50.00; // %
//const float min_hum  =  10.00; // %
//const float min_lux  = 500.00; //LUX

//const float CO2_good = 1000.00; //ppm
//const float CO2_bad  = 2000.00; //ppm

//const float max_CO2
//const float min_CO2









////////////////////////////////////////////////////////////////////
//  GLOBAL VARIABLES                                              //
////////////////////////////////////////////////////////////////////

bool button_state;
/*
unsigned long display_millis;
unsigned long warning_millis;
unsigned long serial_millis;
unsigned long leddim_millis;
unsigned long debounce_millis;
unsigned long humidity_millis;
unsigned long temperature_millis;
unsigned long CO2_millis;

unsigned long Test_millis1 = 0;
unsigned long Test_millis2 = 0;
*/
/*float temperature = 0;
float humidity = 0;
float CO2 = 0;
//LED
int redValue;
int greenValue;
int blueValue;
int menu_index = 1;
bool warning_flag = false;
bool dim_flag = false;
*/

////////////////////////////////////////////////////////////////////
//  CLASSES                                                     //
////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////
//  FUNCTIONS                                                     //
////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////
//      SETUP                                                     //
////////////////////////////////////////////////////////////////////

void display_setup() {
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
}


void sensor_setup() {
  dht.begin();
  co2Serial.begin(BAUDRATE);
  mhz19b.begin(co2Serial);
  mhz19b.autoCalibration();
}

void RTC_init() {
  while (!Serial); // for Leonardo/Micro/Zero
   
    //Serial.begin(BAUDRATE);
    if (! rtc.begin()) {
      Serial.println(F("Couldn't find RTC"));
      while (1);
    }
   
    if (! rtc.isrunning()) {
      Serial.println(F("RTC is NOT running!"));
      rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
      Serial.println(F("RTC adjusted!"));
    }
  }



void LED_setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  //digitalWrite(RED, HIGH);
  //digitalWrite(GREEN, LOW);
  //digitalWrite(BLUE, LOW);
}




////////////////////////////////////////////////////////////////////
//      SENSORS                                                   //
////////////////////////////////////////////////////////////////////
/*
bool read_all_sensor() {
  bool sensor_error = false;
  if (read_humidity() == false) sensor_error = true;
  if (read_temperature() == false) sensor_error = true;
  read_CO2();
  return sensor_error;
}

bool read_humidity() {
  if ((millis() - humidity_millis) > dht11_time){
    if (isnan(dht.readHumidity()) == false) {
      humidity = dht.readHumidity();
      humidity_millis = millis();
      return true;
    }
    else {
      print_error(F("DHT-11 Sensor, humidity!"));
      return false;
   }
  }
}

bool read_temperature() {
  if ((millis() - temperature_millis) > dht11_time){
   if (isnan(dht.readTemperature()) == false) {
    temperature = dht.readTemperature();
    temperature_millis = millis();
    return true;
   }
   else {
     print_error(F("DHT-11 Sensor, temperature!"));
     return false;
   }
  }
}


bool read_CO2() {
  if ((millis() - CO2_millis) > CO2_time) {
    if (mhz19b.errorCode == RESULT_OK) {
      CO2 = mhz19b.getCO2();          //Request CO2 (as ppm)
      CO2_millis = millis();
      return true;
    }
    else {
      print_error(F("MHZ19B Sensor, CO2! "));
      return false;
    }
  }

}
/*
bool warning() {
  bool warning_detected = false;
  //if (threshold (humidity, "Luftfeuchtigkeit", min_hum, max_hum, "%")) warning_detected = true;
  //if ((threshold2 ())== true) warning_detected = true;
  //if (CO2_trafficLight() ) warning_detected = true;
  
  //if (test()) warning_detected = true;
  //if (warning_detected) visualize_warning();
  return warning_detected;
}

*/
/*
bool warning() {
  bool warning_detected = false;
  if (threshold (humidity, "Luftfeuchtigkeit", min_hum, max_hum, "%") ||
      threshold (temperature, "Raumtemperatur", min_temp, max_temp, "\xDF""C")) warning_detected = true;
  else warning_detected = false;
  return warning_detected;
}

bool threshold (float &Parameter, String Parameter_Name, float neg_threshold, float pos_threshold, String unit) {
  if (Parameter > pos_threshold) {
    if ((millis() - serial_millis) > serial_time) {
      Serial.print(F("\nÜberschreitung "));
      Serial.print(Parameter_Name);
      Serial.print(F("!"));
      Serial.print(F("\n Aktueller Wert: "));
      Serial.print(Parameter);
      serial_millis = millis();
    }
    if ((millis() - warning_millis) > warning_time ){
      if (warning_flag) {
        display.clearDisplay();
        display.setCursor(0,10);
        display.print(Parameter_Name);
        display.setCursor(0,20);
        display.print(F("zu hoch!"));
        display.display();
        
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setCursor(0,10);
        display.print(Parameter);
        display.setCursor(0,20);
        display.print(unit);
        display.display();
        warning_flag = true;
      }
      warning_millis = millis();
    }
    return true;
  }
    
  
  if (Parameter < neg_threshold) {
    if ((millis() - serial_millis) > serial_time) {
      Serial.print(F("\nUnterschreitung "));
      Serial.print(Parameter_Name);
      Serial.print(F("!"));
      Serial.print(F("\n Aktueller Wert: "));
      Serial.print(Parameter);
    }
    if ((millis() - warning_millis) > warning_time ){
      if (warning_flag) {
        display.clearDisplay();
        display.print(Parameter_Name);
        display.setCursor(0,10);
        display.print(F("zu niedrig!"));
        display.display();
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setCursor(0,10);
        display.print(Parameter);
        display.print(unit);
        display.display();
        warning_flag = true;
      }
    }
    return true;
  }
  

  else return false;
}

bool CO2_trafficLight() {
  if (CO2 <  CO2_good) {
    
    return false;
  }
  if ((CO2 <= CO2_bad) && (CO2 >= CO2_good)) {
    return false;
  }
  if (CO2 > CO2_bad) return true;
  else  return false;
  
}




/*
bool threshold (float &Parameter, String &Parameter_Name, float &neg_threshold, float &pos_threshold, String &unit) {
  if (Parameter > pos_threshold) {
    //Display
    if ((millis() - warning_millis) > warning_time ){
      if (warning_flag) {
        display.clearDisplay();
        display.setTextSize(1);
        display.setTextColor(SSD1306_WHITE);
        display.print(Parameter_Name);
        display.setCursor(3,1);
        display.print("zu hoch!");
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setTextSize(1);
        display.setTextColor(SSD1306_WHITE);
        display.setCursor(4,0);
        display.print(Parameter);
        display.print(unit);
        warning_flag = true;
      }
      warning_millis = millis();
    }
    return true;
  }
    
  
  if (Parameter < neg_threshold) {
    if ((millis() - warning_millis) > warning_time ){
      if (warning_flag) {
        display.clearDisplay();
        display.print(Parameter_Name);
        display.setCursor(3,1);
        display.print("zu niedrig!");
        warning_flag = false;
      }
      else {
        display.clearDisplay();
        display.setCursor(4,0);
        display.print(Parameter);
        display.print(unit);
        warning_flag = true;
      }
    }
    return true;
  }
  

  else return false;
}

*/
////////////////////////////////////////////////////////////////////
//      LED                                                       //
////////////////////////////////////////////////////////////////////


void visualize_warning() {                                      // not final!
  /*if ((millis() - leddim_millis) > leddim_time) {
      if (dim_flag && redValue < 255-dim_increment){
        if (redValue >= 255-dim_increment) dim_flag = false;
        greenValue = 0;
        blueValue = 0;
        analogWrite(GREEN, greenValue);
        analogWrite(BLUE, blueValue);
        analogWrite(RED, redValue);
        redValue += dim_increment;
      }
    }
    else {
      if (redValue <= dim_increment) dim_flag = true;
      greenValue = 0;
      blueValue = 0;
      analogWrite(GREEN, greenValue);
      analogWrite(BLUE, blueValue);
      analogWrite(RED, redValue);
      redValue -= dim_increment;
    }
  */
        

        /* SOUND-warning should be added here */

}





////////////////////////////////////////////////////////////////////
//      DISPLAY                                                   //
////////////////////////////////////////////////////////////////////
void print_error (String error_message) {
  display.clearDisplay();
  display.print ("Error:" + error_message);
}

/*
bool button_press_menu() {
  bool button_signal = digitalRead(BUTTON_MENU);
  if ((millis() - debounce_millis) > debounce_time) {
    if (button_signal != button_state) {
      button_state = button_signal;
    }
    debounce_millis = millis();
    return button_state;
  }
}
*/
/*
void print_information () {

  if (millis() >= display_millis + display_time || button_press_menu()) {
    if (menu_index > 4) menu_index = 1;
    switch (menu_index) {
      case 1: print_temperature();
      break;
      case 2: print_humidity();
      break;
      case 3: print_date_time();
      break;
      case 4: print_CO2();
      break;
    }
    menu_index++;
    display_millis = millis();
  }
}


void print_temperature() {
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(15,1);
    display.print(F("Raumtemperatur:"));
    display.setTextSize(3);
    display.setCursor(7, 12);
    display.print(temperature);
    display.print((char)247); //° (Degree)
    display.print(F("C"));
    display.display();
    display.clearDisplay();
}
void print_humidity() {
    display.clearDisplay();
    display.setCursor(15,1);
    display.setTextSize(1);
    display.print (F("Luftfeuchtigkeit:"));
    display.setTextSize(3);
    display.setCursor(7, 12);
    display.print(humidity);
    display.print(F("%"));
    display.display();
    display.clearDisplay();
}
void print_date_time() {

    DateTime now = rtc.now();
    display.clearDisplay();
 //Date
    display.setTextSize(2);
    display.setCursor(5, 0);
    if (now.day() < 10) display.print(0);
    display.print(now.day(), DEC);
    display.print(".");
    if (now.month() < 10) display.print(0);
    display.print(now.month(), DEC);
    display.print(".");
    display.print (now.year(),DEC);
//Time
    display.setCursor(15, 18);
    if (now.hour() < 10) display.print(0);
    display.print(now.hour(), DEC);
    display.print(":");
    if (now.minute() < 10) display.print(0);
    display.print(now.minute(), DEC);
    display.print(":");
    if (now.second() < 10) display.print(0);
    display.print(now.second(), DEC);
    
    display.display();
}


void print_CO2() {
  int co2 = CO2;
  display.clearDisplay();
  display.setCursor(25,0);
  display.setTextSize(1);
  display.print(F("CO2-Gehalt: "));
  display.setTextSize(3);
  display.setCursor(2, 12);
  display.print(co2);
  display.print(F("ppm"));
  display.display();
  display.clearDisplay();
}

*/
////////////////////////////////////////////////////////////////////
//  MAIN CODE                                                     //
////////////////////////////////////////////////////////////////////




  String Temp_Name = "Temperatur";
  String Temp_Unit1 = "(char)247)";
  String Temp_Unit2 = "C";

Sensor Temperature(Temp_Name, Temp_Unit1, Temp_Unit2 , min_temp, max_temp, display);



void setup( )
{
  
  Serial.begin( 9600);
  //freeMemory();
  sensor_setup();
  Serial.println(F("solved sensor_setup"));
  display_setup();
  Serial.println(F("solved display_setup"));                             //Setup all Sensors
  //lcd.begin(16, 2);           // set up the LCD's number of columns and rows:
  RTC_init();
  LED_setup();
  pinMode(BUTTON_MENU, INPUT);
  //display_millis = warning_millis = serial_millis = CO2_millis = debounce_millis = humidity_millis = temperature_millis = 0;
  display.clearDisplay();
  display.setCursor(1, 1);
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.print(F("Booting..."));
  display.display();

    
  delay(1000);               // to prevent that everything has enough time to boot up correctly
}


void loop() {
  /*if (read_all_sensor() == false) {                            //If all sensor-values are valid
    
    if (warning() == false) print_information();
  }
  
  */
 
    

  //read_all_sensor();
  //if (warning() == false) print_information();
    
    //Serial.println(Temperature.get_Name());
  //print_information();
  //display.setTextColor(SSD1306_WHITE);
 
  
}

// change