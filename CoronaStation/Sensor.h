//
//  Sensor.hpp
//  HomeOfficeVER2
//
//  Created by Robin Jung on 15.03.21.
//

#ifndef Sensor_hpp
#define Sensor_hpp


#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>

class Sensor {

 public:
  float sensor_value;

    Sensor (String Name, String Unit, Adafruit_SSD1306 &disp);
    Sensor (String Name, String Unit, float Min, float Max, Adafruit_SSD1306 &disp);
    Sensor (String Name, String Unit, String Unit2, float Min, float Max, Adafruit_SSD1306 &disp);
    
    void print();
    bool threshold();
    String get_Name();
    String get_Unit();
    void set_Value(float val);
    float get_Value();
    float get_Max();
    float get_Min();
    
    
private:
    String m_Name;
    String m_Unit;
    float m_Max;
    bool m_Max_b;
    float m_Min;
    bool m_Min_b;
    float m_Value;
    bool m_warning_flag = true;
    Adafruit_SSD1306 &m_display;
    unsigned long m_warning_millis = 0;
    unsigned long m_warning_time = 1000;
};

Sensor::Sensor (String Name, String Unit, Adafruit_SSD1306 &disp) {
    m_Name = Name;
    m_Unit = Unit;
    m_Max_b = false;
    m_Min_b = false;
    m_display = disp;
}

Sensor::Sensor (String Name, String Unit, float Min, float Max, Adafruit_SSD1306 &disp) {
    m_Name = Name;
    m_Unit = Unit;
    m_display = disp;
    if (Min == 0) {
        m_Max = Max;
        m_Max_b = true;
        m_Min_b = false;
    }
    if (Max == 0) {
        m_Min = Min;
        m_Max_b = false;
        m_Min_b = true;
    }
    else {
        m_Max = Max;
        m_Min = Min;
        m_Max_b = true;
        m_Min_b = true;
    }

}

Sensor::Sensor (String Name, String Unit, String Unit2, float Min, float Max, Adafruit_SSD1306 &disp) {
    m_Name = Name;
    m_Unit = Unit + Unit2;
    m_display = disp;
    if (Min == 0) {
        m_Max = Max;
        m_Max_b = true;
        m_Min_b = false;
    }
    if (Max == 0) {
        m_Min = Min;
        m_Max_b = false;
        m_Min_b = true;
    }
    else {
        m_Max = Max;
        m_Min = Min;
        m_Max_b = true;
        m_Min_b = true;
    }

}

void Sensor::print() {
    display.clearDisplay();
    display.setCursor(25,0);
    display.setTextSize(1);
    display.print(m_Name);
    display.setTextSize(3);
    display.setCursor(2, 12);
    display.print(m_Value);
    display.print(m_Unit);
    display.display();
    display.clearDisplay();
}

String Sensor::get_Name() { return m_Name;}
String Sensor::get_Unit() { return m_Unit;}
float Sensor::get_Value() { return m_Value;}
void Sensor::set_Value(float val) { m_Value = val;}
float Sensor::get_Max()   { return m_Max;}
float Sensor::get_Min()   { return m_Min;}



bool Sensor::threshold() {
    bool warning = false;
    if (m_Max_b == true){
        if (m_Value > m_Max) {
            if (m_warning_flag) {
                if ((millis() - m_warning_millis) > m_warning_time ){
                    m_display.clearDisplay();
                    display.setTextSize(1);
                    display.setTextColor(SSD1306_WHITE);
                    m_display.print(m_Name);
                    m_display.setCursor(0,10);
                    m_display.print(F("zu niedrig!"));
                    m_display.display();
                    m_warning_flag = false;
                }
            }
            else {
                if ((millis() - m_warning_millis) > m_warning_time ){
                    m_display.clearDisplay();
                    display.setTextSize(1);
                    display.setTextColor(SSD1306_WHITE);
                    m_display.setCursor(0,10);
                    m_display.print(m_Value);
                    m_display.print(m_Unit);
                    m_display.display();
                    m_warning_flag = true;
                }
            }
            warning = true;
        }
        if (m_Min_b == true) {
            if (m_Value < m_Min) {
                if (m_warning_flag) {
                    if ((millis() - m_warning_millis) > m_warning_time ){
                        m_display.clearDisplay();
                        display.setTextSize(1);
                        display.setTextColor(SSD1306_WHITE);
                        m_display.print(m_Name);
                        m_display.setCursor(0,10);
                        m_display.print(F("zu hoch!"));
                        m_display.display();
                        m_warning_flag = false;
                    }
                }
                else {
                    if ((millis() - m_warning_millis) > m_warning_time ){
                        m_display.clearDisplay();
                        display.setTextSize(1);
                        display.setTextColor(SSD1306_WHITE);
                        m_display.setCursor(0,10);
                        m_display.print(m_Value);
                        m_display.print(m_Unit);
                        m_display.display();
                        m_warning_flag = true;
                    }
                }
                warning = true;
            }
        }
    }
    return warning;
}



#endif /* Sensor_hpp */
